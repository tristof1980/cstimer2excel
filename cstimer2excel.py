# Python program to read CSTimer exported file (JSON format)
# and export it to Excel
#
# Base script comes from this thread:
# https://www.speedsolving.com/threads/my-cstimer-to-excel-python-converter.61912/
# which points to github file:
# https://gist.github.com/anonymous/b221eee608a517e1cc0f1bd9c3835e42

from datetime import datetime, timezone
#import csv
#Check to see if the Computer contains xlsxwriter.
try:
    import json
    import numpy
    import xlsxwriter
except Exception as e:
    print(e)
    exit()
import math
import sys
import os
import subprocess
import platform
_debug = True
countDNF = True
DNF = -1
# If you keep DNFexcel to blank, Excel will not plot those data
# Otherwise, if you write "DNF", Excel will plot 0
DNFexcel = ""

class Create_Excel_Graph():
   def __init__(self):
        if len(sys.argv)>1:
           filename=sys.argv[1]
           if filename == '--help':
              print(os.path.basename(sys.argv[0])+' <filename.txt> <sessionname>')
              print('E.g.: '+os.path.basename(sys.argv[0])+' cstimer.txt session2')
              exit()
        else:
           filename='cstimer.txt'
        if len(sys.argv)>2:
           sessionName=sys.argv[2]
        else:
           #sessionName='Chris-3x3-1_step'
           sessionName=''
        #self.data,sessionName = self.LoadResults(filename,sessionName)
        #self.Excel_Transfer(self.data,sessionName)
        self.LoadResults(filename,sessionName)
   def LoadResults(self,fileName,sessionName):
      # Opening JSON file
      try:
         f = open(fileName)
      except IOError:
         print('Could not open input file: '+fileName)
         exit()
      # returns JSON object as
      # a dictionary
      try:
         cstimer_data = json.load(f)
      except Exception as e:
         print('Error: file format does not seem correct.')
         print('The first character of the file should be "{".')
         print('To export in the correct format, click on CSTimer export button')
         print('in the top-left corner and choose the option "Export to file".')
         print('Then, choose a filename and click on Save')
         #print(e)
         f.close()
         exit()
      # Closing file
      f.close()
      nbsessions=0
      print('List of sessions in CSTimer file:')
      if 'properties' in cstimer_data.keys():
         if 'sessionData' in cstimer_data['properties'].keys():
             sessionData = json.loads(cstimer_data['properties']['sessionData'])
             nbsessions = len(sessionData)
             for i in sessionData:
                if isinstance(sessionData[i]['name'], str):
                   mysessionname=sessionData[i]['name']
                else:
                   mysessionname=str(sessionData[i]['name'])
                if (int(i) < 10):
                   print('Session  ',i,': ',mysessionname)
                else:
                   print('Session ',i,': ',mysessionname)
      if len(sessionName) == 0:
         sessionNames=[]
         for i in range(1,nbsessions+1):
            sessionNames.append(str(i))
      else:
         sessionNames=[sessionName]
      
      #Loop over desired sessions
      for session_i in sessionNames:
          sessionID=-1
          timefactor=1000
          if 'properties' in cstimer_data.keys():
             ##"properties":"useMilli"=true or not?
             #if 'useMilli' in cstimer_data['properties'].keys():
             #   if cstimer_data['properties']['useMilli'] == True:
             #      print('Use of milliseconds')
             #      timefactor=1000
             #Find session name, if defined
             if 'sessionData' in cstimer_data['properties'].keys():
                sessionData = json.loads(cstimer_data['properties']['sessionData'])
                for i in sessionData:
                   if isinstance(sessionData[i]['name'], str):
                      mysessionname=sessionData[i]['name']
                   else:
                      mysessionname=str(sessionData[i]['name'])
                   if session_i == mysessionname:
                      sessionID='session'+i
                   elif session_i == i:
                      sessionID='session'+i
                      session_i=mysessionname
          if sessionID == -1:
             print('Error, could not find session name '+session_i)
             continue
          #allocate AllData[nbcol][nbtimes] to 0
          nbtimes=len(cstimer_data[sessionID])
          if _debug: print(f"Session {session_i} contains {nbtimes} times")
          nbcol=2
          data = numpy.zeros((nbcol,nbtimes))
          
          # Iterating through the json
          # list
          #         time*1000    scramble                                       comment   date
          # i = [[0,28932],"F D F2 L2 U2 F L2 F' L2 B' U2 B F' D B' L F L R' B","",1634047791]
          #       |___ penalty (or -1 for DNF)
          # If multi-steps, the cumulated times (since the beginning) of each step is saved in reverse order.
          # For instance:
          #       [0,65012,56698,42549,6477]
          # means that the individual steps are: (1) 6.477" + (2) 36.072" + (3) 14.149" + (4) 8.314 
          #                                    = (total) 65.012" = 1'05.012"
          #Unix time = number of seconds since Epoch (1/1/1970)
          #For instance: 1634070976 = 2021-10-12 20:36:16
          #print(datetime.fromtimestamp(1634070976))
          #Excel time = number of days since 1/1/1900
          #(1634070976/86400)+25569
          count=0
          for i in cstimer_data[sessionID]:
              if count==0:
                  if len(i[0]) != 2:
                      print(f"Warning: session {session_i} contains times recorded with more than one step")
                      print(f"         Only the sum of all steps will be used in statistics.")
              mytime=i[0][1]
              if i[0][0] != -1:
                 mytime = mytime + i[0][0]
              elif countDNF:
                 mytime = DNF*timefactor
              else:
                 continue
              mydate=i[3]
              #convert date from Unix to XLS
              data[0,count]=(mydate/86400.)+25569.0
              #convert time from milli-seconds to seconds
              data[1,count]=mytime/timefactor
              #print(mytime/timefactor,datetime.fromtimestamp(mydate))
              # write the data to CSV file
              count+=1
          data=data[:,range(count)]
          inds = numpy.argsort(data[0])
          data=data[:,inds]
          #Remove the time of the solver and only keep the date? uncomment next line
          #data[0]=numpy.floor(data[0])
          self.Excel_Transfer(data,session_i)
      return
    
   #Source: https://stackoverflow.com/questions/21599809/python-xlsxwriter-set-border-around-multiple-cells
   def add_to_format(self, existing_format, dict_of_properties, workbook):
      """Give a format you want to extend and a dict of the properties you want to
      extend it with, and you get them returned in a single format"""
      new_dict={}
      for key, value in existing_format.__dict__.iteritems():
         if (value != 0) and (value != {}) and (value != None):
               new_dict[key]=value
      del new_dict['escapes']

      return(workbook.add_format(dict(new_dict.items() + dict_of_properties.items())))
   
   # Format cell borders via a configurable RxC box
   def draw_frame_border(self, workbook, worksheet, first_row, first_col, rows_count, cols_count, thickness=1):

    if cols_count == 1 and rows_count == 1:
        # whole cell
        worksheet.conditional_format(first_row, first_col,
                                     first_row, first_col,
                                     {'type': 'formula', 'criteria': 'True',
                                     'format': workbook.add_format({'top': thickness, 'bottom':thickness,
                                                                    'left': thickness,'right':thickness})})    
    elif rows_count == 1:
        # left cap
        worksheet.conditional_format(first_row, first_col,
                                 first_row, first_col,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'top': thickness, 'left': thickness,'bottom':thickness})})
        # top and bottom sides
        worksheet.conditional_format(first_row, first_col + 1,
                                 first_row, first_col + cols_count - 2,
                                 {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'top': thickness,'bottom':thickness})})

        # right cap
        worksheet.conditional_format(first_row, first_col+ cols_count - 1,
                                 first_row, first_col+ cols_count - 1,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'top': thickness, 'right': thickness,'bottom':thickness})})

    elif cols_count == 1:
        # top cap
        worksheet.conditional_format(first_row, first_col,
                                 first_row, first_col,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'top': thickness, 'left': thickness,'right':thickness})})

        # left and right sides
        worksheet.conditional_format(first_row + 1,              first_col,
                                 first_row + rows_count - 2, first_col,
                                 {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'left': thickness,'right':thickness})})

        # bottom cap
        worksheet.conditional_format(first_row + rows_count - 1, first_col,
                                 first_row + rows_count - 1, first_col,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'bottom': thickness, 'left': thickness,'right':thickness})})

    else:
        # top left corner
        worksheet.conditional_format(first_row, first_col,
                                 first_row, first_col,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'top': thickness, 'left': thickness})})

        # top right corner
        worksheet.conditional_format(first_row, first_col + cols_count - 1,
                                 first_row, first_col + cols_count - 1,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'top': thickness, 'right': thickness})})

        # bottom left corner
        worksheet.conditional_format(first_row + rows_count - 1, first_col,
                                 first_row + rows_count - 1, first_col,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'bottom': thickness, 'left': thickness})})

        # bottom right corner
        worksheet.conditional_format(first_row + rows_count - 1, first_col + cols_count - 1,
                                 first_row + rows_count - 1, first_col + cols_count - 1,
                                 {'type': 'formula', 'criteria': 'True',
                                  'format': workbook.add_format({'bottom': thickness, 'right': thickness})})

        # top
        worksheet.conditional_format(first_row, first_col + 1,
                                     first_row, first_col + cols_count - 2,
                                     {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'top': thickness})})

        # left
        worksheet.conditional_format(first_row + 1,              first_col,
                                     first_row + rows_count - 2, first_col,
                                     {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'left': thickness})})

        # bottom
        worksheet.conditional_format(first_row + rows_count - 1, first_col + 1,
                                     first_row + rows_count - 1, first_col + cols_count - 2,
                                     {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'bottom': thickness})})

        # right
        worksheet.conditional_format(first_row + 1,              first_col + cols_count - 1,
                                     first_row + rows_count - 2, first_col + cols_count - 1,
                                     {'type': 'formula', 'criteria': 'True', 'format': workbook.add_format({'right': thickness})})

   def Excel_Transfer(self, Final_Array,sessionName):
        #Write the Excel File with the Solve Data.
        outfilename=os.getcwd()+os.path.sep+sessionName+'.xlsx'
        try:
           Excel_File = xlsxwriter.Workbook(outfilename)
        except xlsxwriter.exceptions.FileCreateError as e:
           print("Exception caught in opening file: %s\n"
                 "Please close the file if it is open in Excel.\n" % e)
           exit()        
        Excel_Worksheet = Excel_File.add_worksheet("Data")
        Excel_Worksheet.set_column('A:N', 12) # Default column width
        Excel_Worksheet.set_column('A:A', 6)  # Num
        Excel_Worksheet.set_column('B:B', 14) # Date
        Excel_Worksheet.set_column('I:I', 6)  # Num
        Excel_Worksheet.set_column('J:J', 14) # Date
        Excel_Worksheet.set_column('L:L', 6)  # Num
        Excel_Worksheet.set_column('M:M', 14) # Date
        #
        columns_title_format = Excel_File.add_format({'bold': True, 'bg_color': 'blue', 'font_color': 'white', 'align': 'center'})
        best_values_header_format = Excel_File.add_format({'italic': True, 'font_color': 'red', 'align': 'center', 'num_format': '0.000'})
        best_date_header_format = Excel_File.add_format({'italic': True, 'font_color': 'red', 'num_format': 'dd/mm/yyyy', 'align': 'center'})
        date_format = Excel_File.add_format({'num_format': 'dd/mm/yyyy', 'align': 'center'})
        values_format = Excel_File.add_format({'align': 'center', 'num_format': '0.000'})
        integer_format = Excel_File.add_format({'align': 'center', 'num_format': '0'})
        
        #Write the num
        Excel_Worksheet.write('A1', 'Num', columns_title_format)
        Excel_Worksheet.write('A2', 'Best', best_values_header_format) # Best Single label
        Excel_Worksheet.write_column('A3', range(1,len(Final_Array[0])+1), integer_format)
        #Write the dates
        Excel_Worksheet.write('B1', 'Dates', columns_title_format)
        Excel_Worksheet.write_column('B3', Final_Array[0], date_format)
        #
        #Write the Single Solves
        Excel_Worksheet.write('C1', 'Single Solves', columns_title_format)
        Single_solve = Final_Array[1].tolist()
        for index in range(len(Single_solve)):
           if Single_solve[index] == DNF:
              Single_solve[index] = DNFexcel
        Excel_Worksheet.write_column('C3', Single_solve, values_format)
        #
        #Compute single PB evolution
        PB_Array = self.Find_PB(Final_Array)
        Excel_Worksheet.write('I1', 'Num', columns_title_format)
        Excel_Worksheet.write_column('I2', PB_Array[0], integer_format)
        Excel_Worksheet.write('J1', 'Dates', columns_title_format)
        Excel_Worksheet.write_column('J2', PB_Array[1], date_format)
        Excel_Worksheet.write('B2', PB_Array[1,-1],best_date_header_format) # Best Single date
        Excel_Worksheet.write('K1', 'Best Single', columns_title_format)
        Excel_Worksheet.write_column('K2', PB_Array[2], values_format)
        Excel_Worksheet.write('C2', PB_Array[2,-1], best_values_header_format) # Best Single time
        # Border
        border_thickness = 2
        self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 8, 1, 3, border_thickness) # header border
        self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 8, len(PB_Array[0])+1, 3, border_thickness) # table border
        
        data_nb_columns = 3
        #Write the Average of 5
        if len(Final_Array[1]) >= 5:
            data_nb_columns += 1
            Average_Of_Five, PB  = self.Find_Average_Of(5, Final_Array[1])
            Excel_Worksheet.write('D1', 'Average of 5', columns_title_format)
            Excel_Worksheet.write_column('D7', Average_Of_Five, values_format)
            #
            #Compute average PB evolution
            PBavg_Array = self.Find_PBavg(Final_Array, 5, Average_Of_Five)
            Excel_Worksheet.write('L1', 'Num', columns_title_format)
            Excel_Worksheet.write_column('L2', PBavg_Array[0], integer_format)
            Excel_Worksheet.write('M1', 'Dates', columns_title_format)
            Excel_Worksheet.write_column('M2', PBavg_Array[1],date_format)
            Excel_Worksheet.write('D2', PB, best_values_header_format) # Best ao5 time
            Excel_Worksheet.write('N1', 'Best Ao5', columns_title_format)
            Excel_Worksheet.write_column('N2', PBavg_Array[2], values_format)
            # Border
            self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 11, 1, 3, border_thickness) # header border
            self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 11, len(PBavg_Array[0])+1, 3, border_thickness) # table border


        #Write the Average of 12
        if len(Final_Array[1]) >= 12:
            data_nb_columns += 1
            Average_Of_Twelve, PB = self.Find_Average_Of(12, Final_Array[1])
            Excel_Worksheet.write('E1', 'Average of 12', columns_title_format)
            Excel_Worksheet.write_column('E14', Average_Of_Twelve, values_format)
            Excel_Worksheet.write('E2', PB, best_values_header_format) # Best ao12 time

        #Write the Average of 50
        if len(Final_Array[1]) >= 50:
            data_nb_columns += 1
            Average_Of_Fifty, PB = self.Find_Average_Of(50, Final_Array[1], 3)
            Excel_Worksheet.write('F1', 'Average of 50', columns_title_format)
            Excel_Worksheet.write_column('F52', Average_Of_Fifty, values_format)
            Excel_Worksheet.write('F2', PB, best_values_header_format) # Best ao50 time

        #Write the Average of 100
        if len(Final_Array[1]) >= 100:
            data_nb_columns += 1
            Average_Of_Hundred, PB = self.Find_Average_Of(100, Final_Array[1], 5)
            Excel_Worksheet.write('G1', 'Average of 100', columns_title_format)
            Excel_Worksheet.write_column('G102', Average_Of_Hundred, values_format)
            Excel_Worksheet.write('G2', PB, best_values_header_format) # Best ao50 time

        #Write the Average of 1000
        if len(Final_Array[1]) >= 1000:
            data_nb_columns += 1
            Average_Of_Thousand, PB = self.Find_Average_Of(1000, Final_Array[1], 50)
            Excel_Worksheet.write('H1', 'Average of 1000', columns_title_format)
            Excel_Worksheet.write_column('H1002', Average_Of_Thousand, values_format)
            Excel_Worksheet.write('H2', PB, best_values_header_format) # Best ao50 time
        
        nbtimes=len(Final_Array[1])
        # Border around full data
        self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 0, 2, data_nb_columns, border_thickness) # header border
        self.draw_frame_border(Excel_File, Excel_Worksheet, 0, 0, nbtimes+2, data_nb_columns, border_thickness) # table border

        #Build the two charts
        #for i in range(1):
        for i in range(2):
           if i==0:
              Chart_Sheet = Excel_File.add_chartsheet('num')
           else:
              Chart_Sheet = Excel_File.add_chartsheet('dates')
           Chart = Excel_File.add_chart({'type': 'scatter'})
           
           #Single Solve + linear time trend
           #--------------------------------
           #Help on dash_type? https://xlsxwriter.readthedocs.io/working_with_charts.html#chart-formatting-line
           mydict={
               'categories': '=Data!$A$3:$A{}'.format(nbtimes+1),
               'values'    : '=Data!$C$3:$C{}'.format(nbtimes+1),
               'name'      : '=Data!$C$1',
               'line'      : {'color': 'blue','dash_type': 'solid','width': 1},
               'marker'    : {'type': 'none'},
               'trendline' : {'type': 'linear',
                             'name': 'Single Solve Trend',
                             'line': {
                                 'color': 'black',
                                 'width': 3,
                                 }
                             },
               }
           if i==1: mydict['categories']='=Data!$B$3:$B{}'.format(nbtimes+1)
           Chart.add_series(mydict)
           #Best Single (PB)
           #----------------
           mydict={
               'categories': '=Data!$I$2:$I{}'.format(nbtimes+1),
               'values'    : '=Data!$K$2:$K{}'.format(nbtimes+1),
               'name'      : '=Data!$K$1',
               'marker'    : {'type'  : 'square',
                              'size'  : 7,
                              'border': {'color': 'black'},
                              'fill'  : {'color': 'orange'}},
               'line'      : {'color'    : 'orange',
                              'dash_type': 'square_dot',
                              'size'     : 2},
               }
           if i==1: mydict['categories']='=Data!$J$2:$J{}'.format(nbtimes+1)
           Chart.add_series(mydict)
           #Average of 5
           #------------
           if len(Final_Array[1]) >= 5:
               mydict={
                   'categories': '=Data!$A$7:$A{}'.format(nbtimes+1),
                   'values'    : '=Data!$D$7:$D{}'.format(nbtimes+1),
                   'name'      : '=Data!$D$1',
                   #                            dark yellow
                   'line'      : {'color'    : '#EADF00',
                                  'dash_type': 'solid',
                                  'size'     : 2},
                   'marker'    : {'type': 'none'},
                   }
               if i==1: mydict['categories']='=Data!$B$6:$B{}'.format(nbtimes+1)
               Chart.add_series(mydict)
               #Best Ao5 (PB)
               #----------------
               mydict={
                   'categories': '=Data!$L$2:$L{}'.format(nbtimes+1),
                   'values'    : '=Data!$N$2:$N{}'.format(nbtimes+1),
                   'name'      : '=Data!$N$1',
                   'marker'    : {'type'  : 'square',
                                  'size'  : 7,
                                  'border': {'color': 'black'},
                                  'fill'  : {'color': 'red'}},
                   'line'      : {'color'    : 'red',
                                  'dash_type': 'square_dot',
                                  'size'     : 2},
                   }
               if i==1: mydict['categories']='=Data!$M$2:$M{}'.format(nbtimes+1)
               Chart.add_series(mydict)
           #Average of 12
           #-------------
           if len(Final_Array[1]) >= 12:
               mydict={
                   'categories': '=Data!$A$14:$A{}'.format(nbtimes+1),
                   'values'    : '=Data!$E$14:$E{}'.format(nbtimes+1),
                   'name'      : '=Data!$E$1',
                   'line'      : {'color'    : 'orange',
                                  'dash_type': 'solid',
                                  'size'     : 2},
                   'marker'    : {'type': 'none'},
                   }
               if i==1: mydict['categories']='=Data!$B$13:$B{}'.format(nbtimes+1)
               Chart.add_series(mydict)
           #Average of 50
           #-------------
           if len(Final_Array[1]) >= 50:
               mydict={
                   'categories': '=Data!$A$52:$A{}'.format(nbtimes+1),
                   'values'    : '=Data!$F$52:$F{}'.format(nbtimes+1),
                   'name'      : '=Data!$F$1',
                   'line'      : {'color'    : 'red',
                                  'dash_type': 'solid',
                                  'size'     : 2},
                   'marker'    : {'type': 'none'},
                   }
               if i==1: mydict['categories']='=Data!$B$51:$B{}'.format(nbtimes+1)
               Chart.add_series(mydict)
           #Average of 100
           #--------------
           if len(Final_Array[1]) >= 100:
               mydict={
                   'categories': '=Data!$A$102:$A{}'.format(nbtimes+1),
                   'values'    : '=Data!$G$102:$G{}'.format(nbtimes+1),
                   'name'      : '=Data!$G$1',
                   'line'      : {'color'    : 'purple',
                                  'dash_type': 'solid',
                                  'size'     : 2},
                   'marker'    : {'type': 'none'},
                   }
               if i==1: mydict['categories']='=Data!$B$101:$B{}'.format(nbtimes+1)
               Chart.add_series(mydict)
           #Average of 1000
           #---------------
           if len(Final_Array[1]) >= 1000:
               mydict={
                   'categories': '=Data!$A$1002:$A{}'.format(nbtimes+1),
                   'values'    : '=Data!$H$1002:$H{}'.format(nbtimes+1),
                   'name'      : '=Data!$H$1',
                   'line'      : {'color'    : 'black',
                                  'dash_type': 'solid',
                                  'size'     : 2},
                   'marker'    : {'type': 'none'},
                   }
               if i==1: mydict['categories']='=Data!$B$1002:$B{}'.format(nbtimes+1)
               Chart.add_series(mydict)
           if i==1:
              Chart.set_x_axis({
                 'date_axis':       True,
                 #'minor_unit':      4,
                 #'minor_unit_type': 'months',
                 #'major_unit':      1,
                 #'major_unit_type': 'years',
                 'num_format':      'dd/mm/yyyy',
                 })
           Chart.set_legend({'position': 'bottom'})
           Chart_Sheet.set_chart(Chart)
        

        while True:
            try:
                Excel_File.close()
            except xlsxwriter.exceptions.FileCreateError as e:
                decision = input("Exception caught in workbook.close(): %s\n"
                                 "Please close the file if it is open in Excel.\n"
                                 "Try to write file again? [Y/n]: " % e)
                if decision != 'n':
                    continue        
            break
        if os.path.exists(outfilename):
           print('   Generated file '+outfilename)
           value = input("   Do you want to open it ([0]/1)? ")
           if (len(value)==0):
              value="0"
           if (value=="1"):
              if platform.system() == 'Darwin':       # macOS
                 subprocess.call(('open', outfilename))
              elif platform.system() == 'Windows':    # Windows
                 os.startfile(outfilename)
              else:                                   # linux variants
                 subprocess.call(('xdg-open', outfilename))
        else:
           print('Error, no file was generated')

   def Find_Average_Of(self, Average_Number, Time_Array, Times_To_Cancel = 1):
        #Get the Averages and Append them to an Array
        Average_Array = []
        PB = 1e4
        for Time_Index in range(Average_Number, len(Time_Array) + 1):
            Average = 0
            NbDNF = 0
            Smallest_Number_Array = []
            Highest_Number_Array  = [0 for Fillers in range(Times_To_Cancel)]
            Start = True
            for Added_Values in range(1, Average_Number + 1):
                Current_Number = float(Time_Array[Time_Index - Added_Values])
                if Current_Number == DNF:
                   NbDNF += 1
                   Current_Number = 1e4
                if Start:
                    Smallest_Number_Array.append(Current_Number)
                    if len(Smallest_Number_Array) >= Times_To_Cancel:
                        Start = False
                for Times in range(len(Smallest_Number_Array)):
                    if Current_Number < Smallest_Number_Array[Times]:
                        High_Index = Smallest_Number_Array.index(max(Smallest_Number_Array))
                        Smallest_Number_Array[High_Index] = Current_Number
                        break
                for Times in range(len(Highest_Number_Array)):
                    if Current_Number > Highest_Number_Array[Times]:
                        Lowest_Index = Highest_Number_Array.index(min(Highest_Number_Array))
                        Highest_Number_Array[Lowest_Index] = Current_Number
                        break
                Average += Current_Number

            if NbDNF > Times_To_Cancel:
               Average = DNFexcel
            else:
               Small_Total = sum(Smallest_Number_Array)
               High_Total  = sum(Highest_Number_Array)
               Average -= (Small_Total + High_Total)
               Average = round((Average/(Average_Number -
                                              (Times_To_Cancel * 2))),3) # Round to 3 decimals
               if Average < PB:
                  PB = Average
            Average_Array.append(Average)
        return Average_Array, PB
   
   def Find_PB(self, Time_Array):
        #Get the Averages and Append them to an Array
        PB=max(Time_Array[1,:])
        if PB == DNF:
           PB_Array = numpy.zeros((3,1))
           PB_Array[0,0]=1
           PB_Array[1,0]=Time_Array[0,0]
           PB_Array[2,0]=DNFexcel
        else:
           PB += 10.
        PB_Array = numpy.zeros((3,len(Time_Array[0])))
        count=0
        for Time_Index in range(len(Time_Array[0])):
            if Time_Array[1,Time_Index] != DNF and Time_Array[1,Time_Index]<PB:
               PB=Time_Array[1,Time_Index]
               PB_Array[0,count]=Time_Index+1
               PB_Array[1,count]=Time_Array[0,Time_Index]
               PB_Array[2,count]=PB
               count+=1
        PB_Array=PB_Array[:,range(count)]
        return PB_Array
    
   def Find_PBavg(self, Time_Array, Average_Number, Avg_array):
        #Get the Averages and Append them to an Array
        PB=Avg_array[0]
        if PB == DNFexcel:
           PBavg_Array = numpy.zeros((3,1))
           PBavg_Array[0,0]=1
           PBavg_Array[1,0]=Time_Array[0,Average_Number]
           PBavg_Array[2,0]=DNF
           PB = 1000.
        else:
           PB += 10.
        PBavg_Array = numpy.zeros((3,len(Avg_array)))
        count=0
        for Time_Index in range(len(Avg_array)):
            if Avg_array[Time_Index] != DNFexcel and Avg_array[Time_Index]<PB:
               PB=Avg_array[Time_Index]
               PBavg_Array[0,count]=Time_Index+Average_Number
               PBavg_Array[1,count]=Time_Array[0,Time_Index+Average_Number-1]
               PBavg_Array[2,count]=PB
               count+=1
        PBavg_Array=PBavg_Array[:,range(count)]
        return PBavg_Array

if __name__ == '__main__':
    Create_Excel_Graph()
