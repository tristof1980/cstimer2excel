# cstimer2excel

Python program to read CSTimer exported file (JSON format) and convert it to Excel.
This requires Python 3 and packages:
- json
- xlsxwriter
and a few standard packages (numpy, os,...).

# Usage:
cstimer2excel.py [filename.txt [sessionname]]
with default filename.txt = cstimer.txt
If sessionname is omitted, the script will loop over all of them.

# Output:
The script will generate one Excel workbook for every session.

Each workbook will contain the raw data and 2 graphs:
- in the first one, the abscissae will the the solve number
- in the second one, the abscissae will the the solve date (and time)

Both graphs will contain:
- single solve
- personnal best (PB)
- average of 5 (ignoring best and worst time)
- average of 12 (ignoring best and worst time)
- average of 50 (ignoring 3 best and worst times)
- average of 100 (ignoring 5 best and worst times)
- average of 1000 (ignoring 50 best and worst times)

DNF are treated correctly. If a DNF is not ignored and considered in an average,
the average value will be a DNF and the line in the plot will be discontinuous.
DNF are written as blanks in the Excel raw data
