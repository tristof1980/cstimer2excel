@echo off
REM Does not work in the SendTo folder
SET CSTIMERDIR=%~d0%~p0
if [%1]==[] GOTO Help
cd %~p1
REM Examples:
rem python.exe %CSTIMERDIR%\cstimer2excel.py "%1" "Chris-3x3-Full"
rem python.exe %CSTIMERDIR%\cstimer2excel.py "%1" "5"
rem Loop over all sessions
python.exe %CSTIMERDIR%\cstimer2excel.py "%1"
PAUSE
Exit /B 0

:Help
ECHO.You need to run this script with at least one argument
ECHO.containing the full path of the cstimer.txt file to convert
ECHO.It will assume you need to convert all sessions